#include <iostream>
#include "container.h"
#include "Matr.h"
using namespace std;
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	
	Array arr(4);
	for (int i = 0; i < 4; i++)
		arr.insert(i + 1);
	cout<<arr<<endl;
	for (int i = 0; i < 8; i+=2)
		arr.insert(10+i,i);
	cout<<arr<<endl;
	for (int i = 1; i < 8; i+=2)
		arr[i]=20 + i;
	cout<<arr<<endl;
	for (int i=6; i>=0; i-=3)
		arr.remove(i);
	cout<<arr<<endl;
	cout<<"lol"<<endl;
	Array arr1(5);
	for (int i = 0; i < 5; i++)
		arr1.insert(5-i);
	cout<<arr+arr1<<endl;
	cout<<arr-arr1<<endl;
	cout<<arr<<"\n"<<arr*2<<endl;
	cout<<arr * arr1<<endl;
	
	Matr mat(5,5);
	for (int i = 0; i < 5; i++)
	{
		Array arr(5);
		for (int j = 0; j < 5; j++)
		{
			arr.insert (j + 1 + (i+1)*10);
		}
		mat.insertLine(arr);
	}
	cout<<mat<<endl;
	Array arr2(5);
	for (int i = 0; i < 5; i++)
		arr2.insert(i+1);
	mat.insertCol(arr2,3);
	cout<<mat<<endl;
	arr2.insert(6);
	mat.insertLine(arr2,3);
	cout<<mat<<endl;
	
	Matr mat2(6,6);
	for (int i = 0; i < 6; i++)
	{
		Array arr(6);
		for (int j = 0; j < 6; j++)
		{
			arr.insert (j + i);
		}
		mat2.insertLine(arr);
	}
	cout<<mat2<<endl;
	cout<<mat + mat2<<endl;
	cout<<mat - mat2<<endl;
	return 0;
}