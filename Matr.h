#ifndef _MATRIX_H
#define _MATRIX_H


#include "container.h"
#include <iostream>
using namespace std;

const int DEFAULT_CAPX = 5;
const int DEFAULT_CAPY = 5;

class MatrException{};

class Matr
{
	void increaseCapY(int newCapY);
	void increaseCapX(int newCapX);
	
	public:
	int sizex;
	int sizey;
	int capx;
	int capy;
	Array* ptr;
	explicit Matr (int startWidth=DEFAULT_CAPX, int startHeight=DEFAULT_CAPY);
	Matr (const Matr& mat);
	~Matr();
	Matr& operator = (const Matr& mat);
	Array& operator[](int index);
	
	void insertLine(Array elem, int posy);
	void insertLine(Array elem);
	void insertCol(Array elem, int posx);
	void insertCol(Array elem);
	
	Matr& operator += (const Matr& mat);
	Matr operator + (const Matr& mat) const;
	Matr& operator -= (const Matr& mat);
	Matr operator - (const Matr& mat) const;
	friend ostream& operator << (ostream& out, const Matr& mat);
};

#endif
