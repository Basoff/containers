#include "container.h"
Array::Array(int startCapacity)
{
	if (startCapacity <= 0)
		capacity = DEFAULT_CAPACITY;
	else
		capacity = startCapacity;
	ptr = new int[capacity];
	size = 0;
}

Array::Array(const Array &arr)
{
	ptr = new int[arr.capacity];
	size = arr.size;
	capacity = arr.capacity;
	for (int i=0; i<size; i++)
		ptr[i]=arr.ptr[i];
}

Array::~Array()
{
	delete[] ptr;
}

Array& Array::operator = (const Array& arr)
{
	if (this == &arr)
		return *this;
	if (capacity != arr.capacity)
	{
		delete[] ptr;
		ptr = new int[arr.capacity];
		capacity = arr.capacity;
	}
	size = arr.size;
	for (int i = 0; i < size; i++)
		ptr[i] = arr.ptr[i];
	return *this;
}

int& Array::operator [] (int index)
{
	if (index >= size || index < 0)
		throw ArrayException();
	else
		return ptr[index];
}

void Array::insert(int elem, int index)
{
	if (index < 0 || index > size)
		throw ArrayException();
	if (size ==capacity)
		increaseCapacity(size + 1);
	for (int j=size-1; j >= index; j--)
		ptr[j+1]=ptr[j];
	size++;
	ptr[index]=elem;
}

void Array::insert(int elem)
{
	insert(elem, size);
}

void Array::increaseCapacity(int newCapacity)
{
	capacity = newCapacity < capacity*2 ?
		capacity*2: newCapacity;
	int* newPtr = new int[capacity];
	for (int i = 0; i < size; i++)
		newPtr[i] = ptr[i];
	delete[] ptr;
	ptr = newPtr;
}

void Array::remove(int index)
{
	if (index < 0 || index >= size)
		throw ArrayException();
	for (int j = index; j < size - 1; j++)
		ptr[j]=ptr[j+1];
	ptr[size-1]=0;
	size--;
}

int Array::getSize() const
{
	return size;
}

ostream& operator << (ostream& out, const Array& arr)
{
	for (int i = 0; i < arr.size; i++)
		out<<arr.ptr[i]<<"	";
	out<<endl;
	return out;
}

Array& Array::operator += (const Array& arr)
{
	if (size != arr.size)
		throw ArrayException();
	for (int i = 0; i < arr.size; i++)
	{
		ptr[i] += arr.ptr[i];
	}
	return *this;
}

Array Array::operator + (const Array& arr) const
{
	Array res(*this);
	return res += arr;
}

Array& Array::operator -= (const Array& arr)
{
	if (size != arr.size)
		throw ArrayException();
	for (int i = 0; i < arr.size; i++)
		ptr[i] -= arr.ptr[i];
	return *this;
}

Array Array::operator - (const Array& arr) const
{
	Array res(*this);
	return res -= arr;
}    

Array& Array::operator *= (const int num)
{
	
	for (int i = 0; i < size; i++)
		ptr[i] *= num;
	return *this;
}

Array Array::operator * (const int num) const
{
	
	Array res(*this);
	return res *= num;
}

Array& Array::operator *= (const Array& arr)
{
	
	for (int i = 0; i < size; i++)
		ptr[i] *= arr.ptr[i];
	return *this;
}

Array Array::operator * (const Array& arr) const
{
	
	Array res(*this);
	return res *= arr;
}