#ifndef _CONTAINER_H
#define _CONTAINER_H

#include <iostream>
using namespace std;

const int DEFAULT_CAPACITY = 10;

class ArrayException{};

class Array
{
	void increaseCapacity (int newCapacity);	
	
	public:
	int* ptr; //указатель на массив в динам. памяти
	int size;//текущий размер
	int capacity; //вместимость
	explicit Array (int startCapacity=DEFAULT_CAPACITY);
	Array(const Array& arr);
	~Array();
	Array& operator = (const Array& arr);
	int& operator [](int index);
	void insert (int index, int elem);
	void insert (int elem);
	void remove (int index);
	int getSize() const;
	
	Array& operator += (const Array& arr);
	Array operator + (const Array& arr) const;
	Array& operator -= (const Array& arr);
	Array operator - (const Array& arr) const;
	Array& operator *= (const int num);
	Array operator * (const int num) const;
	Array& operator *= (const Array& arr);
	Array operator * (const Array& arr) const;
	friend ostream& operator << (ostream& out, const Array& arr);
};

#endif