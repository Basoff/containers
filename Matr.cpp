#include "Matr.h"

Matr::~Matr()
{
	delete[] ptr;
}

Matr::Matr(int startWidth, int startHeight)
{
	if (startWidth <= 0)
		capx = DEFAULT_CAPX;
	if (startHeight <= 0)
		capy = DEFAULT_CAPY;
	ptr = new Array[capy];
	sizex = sizey = 0;
}

Matr::Matr(const Matr &mat)
{
	ptr = new Array[mat.capy];
	sizex = mat.sizex;
	sizey = mat.sizey;
	capy = mat.capy;
	capx = mat.capx;
	for (int i = 0; i < sizey; i++)
		ptr[i] = mat.ptr[i];	
} 

Matr& Matr::operator = (const Matr& mat)
{
	if (this == &mat)
		return *this;
	if (capy != mat.capy)
	{
		delete[] ptr;
		ptr = new Array[mat.capy];
		capy = mat.capy;
	}
	sizey = mat.sizey;
	sizex = mat.sizex;
	for (int i = 0; i < sizey; i++)
		ptr[i] = mat.ptr[i];
	return *this;
}

Array& Matr::operator [] (int index)
{
	if (index >= sizey || index < 0)
		throw MatrException();
	else
		return ptr[index];
}

void Matr::increaseCapX(int newCapX)
{
	capx = newCapX < capx*2 ?
		capx*2 : newCapX;
	Array* newPtr = new Array[capy];
	for (int i = 0; i < sizey; i++)
		for (int j = 0; j < sizex; j++)
			newPtr[i][j] = ptr[i][j];
	delete[] ptr;
	ptr = newPtr;
}

void Matr::increaseCapY(int newCapY)
{
	capy = newCapY < capy*2 ?
		capy*2 : newCapY;
	Array* newPtr = new Array[capy];
	for (int i = 0; i < sizey; i++)
		newPtr[i] = ptr[i];
	delete[] ptr;
	ptr = newPtr;
}

void Matr::insertLine (Array elem, int posy)
{
	if (posy < 0 || posy > sizey)
		throw MatrException();
	if (sizey == capy)
		increaseCapY(sizey + 1);
	if (sizex < elem.size)
		sizex = elem.size;
	for (int j = sizey-1; j >= posy; j--)
		ptr[j+1]=ptr[j];
	sizey ++;
	ptr[posy] = elem;
}
void Matr::insertLine(Array elem)
{
	insertLine(elem, sizey);
}

void Matr::insertCol (Array elem, int posx)
{
	if (posx < 0 || posx > sizex)
		throw MatrException();
	if (sizex == capx)
		increaseCapX(sizex + 1);
	sizex ++;
	for (int j = 0; j < sizey; j++)
	{
		ptr[j].insert(elem[j],posx);
	}
}
void Matr::insertCol(Array elem)
{
	insertCol(elem, sizex);
}

Matr& Matr::operator += (const Matr& mat)
{
	if (sizey != mat.sizey)
		throw MatrException();
	for (int i = 0; i < sizey; i++)
	{
		ptr[i] += mat.ptr[i];
	}
	return *this;
} 

Matr Matr::operator + (const Matr& mat) const
{
	Matr r (*this);
	return r += mat;
} 

Matr& Matr::operator -= (const Matr& mat)
{
	if (sizey != mat.sizey)
		throw MatrException();
	for (int i = 0; i < sizey; i++)
		ptr[i] -= mat.ptr[i];
	return *this;
} 

Matr Matr::operator - (const Matr& mat) const
{
	Matr r (*this);
	return r -= mat;
} 

ostream& operator << (ostream& out, const Matr& mat)
{
	out<<"Total size: "<<mat.sizex<<"x"<<mat.sizey<<endl;
	for (int i = 0; i < mat.sizey; i++)
	{
		out<<mat.ptr[i]<<endl;
	}
	return out;
}
